function sumar10numerosWhile (total, i){
	total = 0;
	i = 0;
	while (i < 10) {
		i++;
		total = total + parseInt(prompt("Ingrese 1 número para sumar"));
	}
	alert("La suma total es " + total);
}
function sumar10numerosDoWhile (total, i){
	total = 0;
	i = 0;
	do {
		i++;
		total = total + parseInt(prompt("Ingrese 1 número para sumar"));
	} while (i < 10) ;
	alert("La suma total es " + total);
}
function sumar10numerosFor (total){
	total = 0;
	for (var i = 0; i < 10; i++) {
		total = total + parseInt(prompt("Ingrese 1 número para sumar"));
	}
	alert("La suma total es " + total);
}
// Ejercicio 4 de Edad promedio entre "N" estudiantes
function edadPromedio1 () { //For
	var numeroAlumnos, edad, promedio;
	numeroAlumnos = parseInt(prompt("Ingrese número de estudiantes"));
	edad = 0;
	for (var i = 0; i < numeroAlumnos; i++) {
		edad = edad + parseInt(prompt("Ingresa edad a promediar"));
	}
	promedio = edad / numeroAlumnos;
	alert("La edad promedio entre los " + numeroAlumnos + " alumnos, es " + promedio);
}
function edadPromedio2 () { //While
	var numeroAlumnos, edad, promedio, i;
	numeroAlumnos = parseInt(prompt("Ingrese número de estudiantes"));
	edad = 0;
	i = 0;
	while (i < numeroAlumnos) {
		edad = edad + parseInt(prompt("Ingresa edad a promediar"));
		i++;
	}
	promedio = edad / numeroAlumnos;
	alert("La edad promedio entre los " + numeroAlumnos + " alumnos, es " + promedio);
}
function edadPromedio3 () { //Do While
	var numeroAlumnos, edad, promedio, i;
	numeroAlumnos = parseInt(prompt("Ingrese número de estudiantes"));
	edad = 0;
	i = 0;
	do {
		edad = edad + parseInt(prompt("Ingresa edad a promediar"));
		i++;
	} while (i < numeroAlumnos);
	promedio = edad / numeroAlumnos;
	alert("La edad promedio entre los " + numeroAlumnos + " alumnos, es " + promedio);
}
function numerosPares () {
	var numero, i;
	var pares = [];
	for (i = 0; i <= 100; i++) {
		if (i % 2 == 0) {
			pares.push(i);
		}
	}
	alert("Numeros pares del 0 al 100 " + pares);
}

// FALTA TEOREMA DE PITAGORAS

function tablasDeMultiplicar () {
	var numero, respuesta;
	var mensaje = "";
	numero = parseInt(prompt("Ingrese un numero para conocer su tabla de multiplicar"));
	for (var i = 1; i <= 10; i++) {
		respuesta = numero * i;
		mensaje = mensaje + numero + " x " + i + " = " + respuesta + "\n";
	}
	alert(mensaje);
}
function potencias () {
	var base, exponente, potencia;
	base = parseInt(prompt("Ingrese numero Base"));
	exponente = parseInt(prompt("Ingrese numero Exponente"));
	potencia = base;
	for (var i = 1; i < exponente; i++) {
		potencia = potencia * base;
	}
	alert(base + " elevado a " + exponente + " = " + potencia);
}
function promedio40 () {
	var nota, suma, promedio;
	var notas = [];
	suma = 0;
	for (var i = 0; i < 40; i++) {
		nota = parseInt(prompt("Ingrese nota del alumno " + (i+1)));
		if (nota > 0 && nota <= 7) {
			suma = suma + nota;
			notas.push(nota);
		} else {
			i--;
			alert("Nota ingresa no es válida");
		}
	}
	notas.sort();
	promedio = suma / 40;
	alert("El promedio de los 40 alumnos es " + promedio + " y la nota mas baja es " + notas[0]);
}
function cuboYCuarta () {
	var numero, cubo, cuarta;
	numero = parseInt(prompt("Ingrese numero para obtener el cubo y la cuarta"));
	cubo = numero;
	cuarta = numero;
	for (var i = 1; i <= 2; i++) { //cubo
		cubo = cubo * numero;
	}
	for (var i = 1; i <= 3; i++) { //cuarta
		cuarta = cuarta * numero;
	}
	alert("De " + numero + " , el cubo es " + cubo + " y la cuarta es " + cuarta);
}








