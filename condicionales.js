//Condicionales simples y condicionales múltiples
function notas (n1, n2, n3) {
	n1 = parseInt(prompt("Ingrese la primera nota"));
	n2 = parseInt(prompt("Ingresa la segunda nota"));
	n3 = parseInt(prompt("Ingresa la tercera nota"));
	var promedio;
	promedio = (n1 + n2 + n3)/3;
	if (promedio >= 6) {
		alert("Aprobaste! tu promedio es " + promedio);
	} else {
		alert("Reprobaste u.u Tu promedio es " + promedio);
	}
}
function numeroMayor (n1, n2) {
	n1 = parseInt(prompt("Ingresa el primer número"));
	n2 = parseInt(prompt("Ingresa el segundo número"));
	var mayor;
	if (n1 > n2) {
		alert(n1 + " es mayor");
	} else {
		alert(n2 + " es mayor");
	}
}
function positivoNegativo (n1) {
	n1 = parseInt(prompt("Ingrese el número"));
	if (n1 >= 0) {
		alert(n1 + " es positivo");
	} else {
		alert(n1 + " es negativo");
	}
}
function descuentoAlmacen (valorTraje) {
	valorTraje = parseInt(prompt("Ingrese el valor del traje"));
	if (valorTraje >= 25000) {
		var dcto;
		dcto = valorTraje * 0.15;
		valorTraje = valorTraje - dcto;
		alert("Tu descuento es " + dcto + ". El valor final de tu traje es " + valorTraje);
	} else {
		var dcto;
		dcto = valorTraje * 0.08;
		valorTraje = valorTraje - dcto;
		alert("Tu descuento es " + dcto + ". El valor final de tu traje es " + valorTraje);
	}
}
function numMayor (n1, n2, n3) {
	n1 = parseInt(prompt("Ingrese el primer número"));
	n2 = parseInt(prompt("Ingresa el segundo número"));
	n3 = parseInt(prompt("Ingresa el tercer número"));
	if (n1 > n2 && n1 > n3) {
		alert(n1 + " es el mayor");
	} else if (n2 > n1 && n2 > n3) {
		alert(n2 + " es el mayor");
	} else {
		alert(n3 + " es el mayor");
	}
}
function valorComida (numeroPersonas, valorEventos) {
	numeroPersonas = parseInt(prompt("Ingrese la cantidad de personas"));
	if (199 >= numeroPersonas) {
		valorEventos = numeroPersonas * 9500;
		alert("El valor de su comida es " + valorEventos);
	} else if (numeroPersonas >= 200 && 300 >= numeroPersonas) {
		valorEventos = numeroPersonas * 8500;
		alert("El valor de su comida es " + valorEventos);
	} else {
		valorEventos = numeroPersonas * 7500;
		alert("El valor de su comida es " + valorEventos);
	}
}
function valorPasajeAlumno (numeroAlumnos, valorTotal) {
	numeroAlumnos = parseInt(prompt("Ingrese la cantidad de alumnos"));
	if (numeroAlumnos <= 29) {
		valorTotal = 400000;
		var valorAlumno;
		valorAlumno = valorTotal / numeroAlumnos;
		alert("El valor de cada pasaje es " + valorAlumno + " y el valor total a pagar es " + valorTotal);
	} else if (numeroAlumnos > 29 && numeroAlumnos <= 49) {
		valorTotal = numeroAlumnos * 9500;
		alert("El valor de cada pasaje es $9500 y el valor total a pagar es " + valorTotal);
	} else if (numeroAlumnos > 49 && numeroAlumnos <= 99) {
		valorTotal = numeroAlumnos * 7000;
		alert("El valor de cada pasaje es $7000 y el valor total a pagar es " + valorTotal);
	} else {
		valorTotal = numeroAlumnos * 6500;
		alert("El valor de cada pasaje es $6500 y el valor total a pagar es " + valorTotal);
	}
}
function companiaBuses (kmRecorridos, cantPasajeros, ppA, ppB, ppC, valorPpA, valorPpB, valorPpC, totalA, totalB, totalC) {
	kmRecorridos = parseInt(prompt("Ingrese el total de kilómetros a recorrer"));
	cantPasajeros = parseInt(prompt("Ingrese la cantidad de pasajeros"));
	ppA = 20;
	ppB = 25;
	ppC = 30;
	if (cantPasajeros <= 20) {
		valorPpA = ppA * kmRecorridos;
		valorPpB = ppB * kmRecorridos;
		valorPpC = ppC * kmRecorridos;
		totalA = valorPpA * 20;
		totalB = valorPpB * 20;
		totalC = valorPpC * 20;
	} else {	
		valorPpA = ppA * kmRecorridos;
		valorPpB = ppB * kmRecorridos;
		valorPpC = ppC * kmRecorridos;
		totalA = valorPpA * kmRecorridos;
		totalB = valorPpB * kmRecorridos;
		totalC = valorPpC * kmRecorridos;
	}
	alert("Buses A: Valor por pasajero " + valorPpA + ". Valor total " + totalA + "\nBuses B: Valor por pasajero " + valorPpB + ". Valor total " + totalB + "\nBuses C: Valores por pasajero " + valorPpC + ". Valor total " + totalC);
}
function hamburguesas (tipoHamburguesa, subTotal, formaPago, total) {
	tipoHamburguesa = parseInt(prompt("Ingrese el tipo de hamburguesa:\n 1 - Simple\n 2 - Doble\n 3 - Triple"));
	if (tipoHamburguesa == 1) {
		subTotal = 2000;
	} else if (tipoHamburguesa == 2) {
		subTotal = 2500;
	} else if (tipoHamburguesa == 3){
		subTotal = 2800;
	} else {
		return alert("Ingrese una opción válida");
	}
	formaPago = parseInt(prompt("Ingrese forma de pago: \n 1 - Efectivo\n 2 - Tarjeta de Credito"));
	if (formaPago == 1) {
		total = subTotal;
		alert("Valor a total a pagar")
	}
	else if (formaPago == 2) {
		total = subTotal * 1.05;
		alert("El valor total a pagar es " + total);
	}
	else {
		return alert("Ingrese una opción válida");
	}
}
function elCometa (codigoProducto, materiaPrima, manoDeObra, gtosFabricacion, ctoProd, pVta) {
	codigoProducto = parseInt(prompt("Ingrese codigo de producto: \n 1 - Producto A\n 2 - Producto B\n 3 - Producto C\n 4 - Producto D\n 5 - Producto E\n 6 - Producto F"));
	materiaPrima = parseInt (prompt("Ingrese valor de materia prima"));
	if (codigoProducto == 1) {
		manoDeObra = materiaPrima * 0.8;
		gtosFabricacion = materiaPrima * 0.28;
		ctoProd = materiaPrima + manoDeObra + gtosFabricacion;
		pVta = (ctoProd * 0.45) + ctoProd;
	} else if (codigoProducto == 2) {
		manoDeObra = materiaPrima * 0.85;
		gtosFabricacion = materiaPrima * 0.30;
		ctoProd = materiaPrima + manoDeObra + gtosFabricacion;
		pVta = (ctoProd * 0.45) + ctoProd;
	} else if (codigoProducto == 3) {
		manoDeObra = materiaPrima * 0.75;
		gtosFabricacion = materiaPrima * 0.35;
		ctoProd = materiaPrima + manoDeObra + gtosFabricacion;
		pVta = (ctoProd * 0.45) + ctoProd;
	} else if (codigoProducto == 4) {
		manoDeObra = materiaPrima * 0.75;
		gtosFabricacion = materiaPrima * 0.28;
		ctoProd = materiaPrima + manoDeObra + gtosFabricacion;
		pVta = (ctoProd * 0.45) + ctoProd;
	} else if (codigoProducto == 5) {
		manoDeObra = materiaPrima * 0.8;
		gtosFabricacion = materiaPrima * 0.30;
		ctoProd = materiaPrima + manoDeObra + gtosFabricacion;
		pVta = (ctoProd * 0.45) + ctoProd;
	} else if (codigoProducto == 6) {
		manoDeObra = materiaPrima * 0.85;
		gtosFabricacion = materiaPrima * 0.35;
		ctoProd = materiaPrima + manoDeObra + gtosFabricacion;
		pVta = (ctoProd * 0.45) + ctoProd;
	} else {
		alert("Ingrese una opción válida");
	}
	alert("Valores de su producto: \n Costo de Pruduccion " + ctoProd + "\n Precio de Venta " + pVta);
}













